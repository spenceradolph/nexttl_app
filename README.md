# Nexttl

Proof of concept for MTTL Refactor

# Getting Started

## Environment

```bash
git clone --recursive https://gitlab.com/spenceradolph/nexttl_app
# (create .env in root of project based on .env.example)
# (use VSCode to open in remote container)
```

---

## Authentication

You may use a local instance of keycloak to authenticate against.

-   add '127.0.0.1 nexttl-keycloak' to your hosts file (your real host)
-   browse to nexttl-keycloak:8080 in a browser & login with creds admin:admin
-   add realm -> `nexttl_realm`
-   create client -> `oauth_client`
    -   Access Type = `confidential`
    -   Authorization Enabled = `ON`
    -   Valid Redirect URIs = `http://localhost:3000/*`
    -   Save
    -   Mappers Tab (add builtin) -> select `realm roles` -> add selected -> edit -> add to ID token
-   copy secret from oauth_client credentials tab (create ./packages/next/.env file and put it there)
-   create roles (`admin` & `vjqr`)
-   create user with password (credentials tab) (can edit roles with 'role mappings' tab) # 'asdf:asdf' is needed for testing!

The main site requires authentication for all pages, but could be disabled / adjusted in `./apps/nextjs-app/src/middleware.ts` and returning true without checks.

## Database

Package scripts assist with this. Generates data from git submodule (nexttl_content)

```bash
yarn workspace @nexttl/database sync      # local postgres DB sync with schema
yarn workspace @nexttl/database seed      # create data
yarn workspace @nexttl/database generate  # rebuild based on schema
yarn workspace @nexttl/database studio    # prisma studio
```

## Development

```bash
yarn workspace @nexttl/app dev              # for the main site
yarn workspace @nexttl/documentation start  # for the docs
```

## Testing

E2E Testing is setup via playwright. Since development is done inside desktop-less containers, tests run in headless mode.
Playwright VSCode extension is installed to assist.

```bash
yarn workspace @nexttl/app test:e2e
```

Playwright also allows `codegen`, which opens a browser and records user interactions to generate a test. This feature only works when run with a desktop environment (not inside our devcontainer).

## Deployment

These are run from the root of the project.

```bash
yarn build           # builds docs, moves them into the app, then builds app
yarn start -p 3000   # same port makes local keycloak still work
```
