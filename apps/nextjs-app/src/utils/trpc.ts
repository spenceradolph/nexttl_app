/**
 * // TODO: Most of this file is copied from TRPC example starters, take care to note options and understand the setup
 * // https://github.com/trpc/examples-v10-next-prisma-starter-sqlite/blob/next/src/utils/trpc.ts
 */

import { httpBatchLink } from '@trpc/client';
import { setupTRPC } from '@trpc/next';
import type { inferProcedureInput, inferProcedureOutput } from '@trpc/server';
import { NextPageContext } from 'next';
import superjson from 'superjson';

import type { AppRouter } from '@nexttl/api';

const getBaseUrl = () => {
	if (typeof window !== 'undefined') return '';

	// TODO: will have to get domain name from ENV when in production
	// ex: https://github.com/trpc/examples-v10-next-prisma-starter-sqlite/blob/next/src/utils/trpc.ts#L15

	// assume localhost
	return `http://localhost:${process.env.PORT ?? 3000}`;
};

export interface SSRContext extends NextPageContext {
	status?: number;
}

export const trpc = setupTRPC<AppRouter, SSRContext>({
	config() {
		return {
			transformer: superjson,
			links: [
				httpBatchLink({
					url: `${getBaseUrl()}/api/trpc`,
				}),
			],
			queryClientConfig: { defaultOptions: { queries: { refetchOnWindowFocus: false, retry: false, staleTime: Infinity } } },
		};
	},

	// TODO: SSR may need some integration with auth, keeping off for now... simplicity
	ssr: false,
});

export type inferQueryOutput<TRouteKey extends keyof AppRouter['_def']['queries']> = inferProcedureOutput<AppRouter['_def']['queries'][TRouteKey]>;
export type inferQueryInput<TRouteKey extends keyof AppRouter['_def']['queries']> = inferProcedureInput<AppRouter['_def']['queries'][TRouteKey]>;
export type inferMutationOutput<TRouteKey extends keyof AppRouter['_def']['mutations']> = inferProcedureOutput<AppRouter['_def']['mutations'][TRouteKey]>;
export type inferMutationInput<TRouteKey extends keyof AppRouter['_def']['mutations']> = inferProcedureInput<AppRouter['_def']['mutations'][TRouteKey]>;

// Syntactic shortening
export const useUtils = trpc.useContext;
export const api = trpc.proxy;
