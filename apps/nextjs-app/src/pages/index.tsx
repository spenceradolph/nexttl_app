import { Box, Container, Stack, Typography } from '@mui/material';
import type { NextPage } from 'next';
import Image from 'next/image';
import { useEffect } from 'react';
import shadowWarrior from '../../public/shadow_warrior.svg';
import { useUtils } from '../utils';

const Home: NextPage = () => {
	const utils = useUtils();

	useEffect(() => {
		utils.prefetchQuery(['ksats.all']);
		utils.prefetchQuery(['vjqrs.myVJQRs']);
		utils.prefetchQuery(['vjqrs.menteeVJQRs']);
	}, [utils]);

	return (
		<>
			<div>Homepage Content</div>
		</>
	);
};

export default Home;
