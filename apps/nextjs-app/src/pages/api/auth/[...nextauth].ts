import { authOptions } from '@nexttl/auth';
import NextAuth from 'next-auth';

export default NextAuth(authOptions);
