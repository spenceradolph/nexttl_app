import { CircularProgress, Paper, TableContainer, Typography } from '@mui/material';
import type { NextPage } from 'next';
import { useEffect, useState } from 'react';
import { MttlTable } from '../components';
import { api, KsatRow, useStore } from '../utils';

const Mttl: NextPage = () => {
	const { data: ksats, error, isLoading } = api.ksats.all.useQuery();
	const storeKsats = useStore(({ ksats }) => ksats);
	const [tableKsats, setTableKsats] = useState<KsatRow[]>([]);

	useEffect(() => {
		if (!ksats || !storeKsats) return;

		const ksatsToDisplay = ksats.map((ksat) => {
			const indexInStore = storeKsats.findIndex(({ id }) => id === ksat.id);
			if (indexInStore === -1) return { ...ksat, isLocalEdit: false };
			return storeKsats[indexInStore];
		});

		setTableKsats(ksatsToDisplay);
	}, [ksats, storeKsats]);

	if (error) return <Typography>{JSON.stringify(error)}</Typography>;
	if (isLoading || !ksats) return <CircularProgress />;

	return (
		<>
			<TableContainer component={Paper}>
				<MttlTable tableKsats={tableKsats} />
			</TableContainer>
		</>
	);
};

export default Mttl;
