import { SessionProvider } from 'next-auth/react';
import { AppType } from 'next/dist/shared/lib/utils';
import Head from 'next/head';
import { Layout } from '../components';
import { trpc } from '../utils';

import '../styles/globals.css';

const MyApp: AppType = ({ Component, pageProps: { session, ...pageProps } }) => {
	return (
		<>
			<Head>
				<title>90 COS Training</title>
				<link rel="icon" href="/shadow_warrior.svg" />
			</Head>
			<SessionProvider session={session}>
				<Layout>
					<Component {...pageProps} />
				</Layout>
			</SessionProvider>
		</>
	);
};

export default trpc.withTRPC(MyApp);
