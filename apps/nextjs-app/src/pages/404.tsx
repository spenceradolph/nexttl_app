import { Box } from '@mui/material';
import type { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import wordCloud from '../../public/90cos-wordcloud-grey.png';

const PageNotFound: NextPage = () => {
	return (
		<>
			<div>
				<Head>
					<title>404 Not Found</title>
				</Head>
				<h2>PAGE NOT FOUND!</h2>
				<h3>Attention Shadow Warrior! You have lost your path through the Aether.</h3>
				<p>The page that you searched for could not be found.</p>
				<p>Please check your spelling.</p>
				<p>If you have clicked on a 90 COS provided link or you believe that you have reached this page in error:</p>
				<p>
					Please contact CYT <a href="https://gitlab.com/90cos/mttl/-/issues/new?issuable_template=Bug">here</a>
				</p>
				<p>
					If you are scraping random pages, please consider improving your parser - cf.{' '}
					<a href="https://stackoverflow.com/questions/24322368/using-beautifulsoup-to-parse-webpages-to-skip-404-error-pages">here</a>
				</p>
				<Box sx={{ height: '30%', width: '30%' }}>
					<Image src={wordCloud} alt="word cloud" />
				</Box>
			</div>
		</>
	);
};

export default PageNotFound;
