import { withAuth } from 'next-auth/middleware';

// https://nextjs.org/docs/advanced-features/middleware
// https://next-auth.js.org/configuration/nextjs#advanced-usage
export default withAuth({
	callbacks: {
		authorized: ({ token, req }) => {
			// if /api or /docs needs unprotected routes, can add exceptions here
			// if (req.nextUrl.pathname.startsWith('/api/')) return true;

			// all pages require authentication
			if (!token) return false;

			// Admin pages super protected
			// if (req.nextUrl.pathname.startsWith('/admin')) return token.isAdmin;

			// VJQR pages extra protected
			// if (req.nextUrl.pathname.startsWith('/vjqr')) return token.isVJQR || token.isAdmin;

			return true;
		},
	},
});
