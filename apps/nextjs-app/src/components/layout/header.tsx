import { AppBar, Box, Button, Toolbar, Typography } from '@mui/material';
import { useSession } from 'next-auth/react';
import Image from 'next/image';
import Link from 'next/link';
import shadowWarrior from '../../../public/shadow_warrior.svg';
import { api, useStore, useUtils } from '../../utils';

export const Header = () => {
	const { data: session } = useSession();
	const [storeKsats, setStoreKsats] = useStore(({ ksats, setKsats }) => [ksats, setKsats]);
	const utils = useUtils();
	const commitMutation = api.ksats.update.useMutation({
		onSuccess: () => {
			setStoreKsats([]);
			utils.invalidateQueries('ksats.all');
		},
	});

	const commitChangesButton =
		storeKsats.length === 0 ? null : (
			<Button onClick={() => commitMutation.mutate({ storeKsats })}>
				<Typography color="inherit">Commit Changes</Typography>
			</Button>
		);

	return (
		<>
			<AppBar position="fixed" sx={{ backgroundColor: 'white', color: 'black' }}>
				<Toolbar sx={{ display: 'flex' }}>
					{/* Left Side */}
					<Image src={shadowWarrior} alt="logo" width="30%" height="30%" />
					<Link href="/">
						<Typography variant="h6" component="div" sx={{ flexGrow: 1, ml: 2, cursor: 'pointer' }}>
							90 COS Training & Evals
						</Typography>
					</Link>

					{/* Right Side */}
					<Box sx={{ display: 'flex', width: '50%', justifyContent: 'space-evenly' }}>
						{commitChangesButton}
						<Link href="/mttl">
							<Button>
								<Typography color="inherit">MTTL</Typography>
							</Button>
						</Link>
						<Link href="/vjqr">
							<Button>
								<Typography color="inherit">VJQRs</Typography>
							</Button>
						</Link>
						<Link href="/docs">
							<Button>
								<Typography color="inherit">DOCS</Typography>
							</Button>
						</Link>
						<Button disabled>
							<Typography color="inherit">Logged In as: {session?.username}</Typography>
						</Button>
					</Box>
				</Toolbar>
			</AppBar>
		</>
	);
};
