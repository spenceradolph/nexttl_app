import { Header } from './header';
import { Header2 } from './header2';

export const Layout = ({ children }: { children: React.ReactNode }) => {
	return (
		<>
			{/* <Header /> */}
			<Header2 />
			<main>{children}</main>
		</>
	);
};
