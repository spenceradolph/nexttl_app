import Image from 'next/image';
import Link from 'next/link';

const navLinks = [
	{ name: 'MTTL', href: '/mttl' },
	{ name: 'VJQRs', href: '/vjqr' },
	{ name: 'Docs', href: '/docs' },
];

export const Header2 = () => {
	return (
		<nav className="h-16 bg-gray-800 mx-auto px-2 flex">
			<div className="h-16 w-16 relative hover:bg-gray-700 cursor-pointer">
				<Link href="/">
					<a>
						<Image
							src="/shadow_warrior.svg"
							alt="Logo"
							layout="fill" // required
							objectFit="fill" // change to suit your needs
							// className="rounded-full" // just an example
						/>
					</a>
				</Link>
			</div>
			<div className="flex justify-left items-center ml-10">
				{navLinks.map((item) => (
					<Link key={item.name} href={item.href}>
						<a className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium text-center">{item.name}</a>
					</Link>
				))}
			</div>
		</nav>
	);
};
