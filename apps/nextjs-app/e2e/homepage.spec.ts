import { test, expect } from '@playwright/test';

// Runs before each test and signs in
// https://playwright.dev/docs/test-auth
test.beforeEach(async ({ page }) => {
	await page.goto('/api/auth/signin');
	await page.locator('text=Sign in with Keycloak').click();

	await page.locator('input[name="username"]').fill('asdf');
	await page.locator('input[name="password"]').fill('asdf');
	await page.locator('input:has-text("Sign In")').click();

	await expect(page).toHaveURL('/');
});

test('homepage has Nexttl in title', async ({ page }) => {
	await page.goto('/');

	await expect(page).toHaveTitle('90 COS Training');
});
