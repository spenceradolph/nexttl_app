import { test, expect } from '@playwright/test';

test('prevent unauthenticated access', async ({ page }) => {
	await page.goto('/');

	await expect(page).not.toHaveURL('/');
	await expect(page).toHaveURL(/\/api\/auth\/signin*/);
});
