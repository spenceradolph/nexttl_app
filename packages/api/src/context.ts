import { authOptions, getServerSession } from '@nexttl/auth';
import { inferAsyncReturnType } from '@trpc/server';
import { CreateNextContextOptions } from '@trpc/server/adapters/next';

// https://trpc.io/docs/context
export async function createContext(opts?: CreateNextContextOptions) {
	const req = opts?.req;
	const res = opts?.res;

	const session = req && res && (await getServerSession(req, res, authOptions));

	return {
		req,
		res,
		session,
	};
}

export type Context = inferAsyncReturnType<typeof createContext>;
