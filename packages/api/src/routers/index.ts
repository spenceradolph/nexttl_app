import { router } from '../trpc';
import { ksatRouter } from './ksats';
import { vjqrRouter } from './vjqr';

export const appRouter = router({
	ksats: ksatRouter,
	vjqrs: vjqrRouter,
});

export type AppRouter = typeof appRouter;
