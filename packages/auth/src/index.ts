import { NextAuthOptions } from 'next-auth';
import KeycloakProvider from 'next-auth/providers/keycloak';

export const authOptions: NextAuthOptions = {
	// https://next-auth.js.org/providers/keycloak
	// https://github.com/nextauthjs/next-auth/issues/4006
	providers: [
		KeycloakProvider({
			clientId: process.env.KEYCLOAK_ID || '',
			clientSecret: process.env.KEYCLOAK_SECRET || '',
			issuer: process.env.KEYCLOAK_ISSUER || '',
		}),
	],

	// https://next-auth.js.org/configuration/callbacks
	callbacks: {
		async jwt({ token, user, account, profile, isNewUser }) {
			profile && (token.username = profile.preferred_username);
			profile && (token.email = profile.email);
			profile && (token.isAdmin = profile.realm_access.roles.includes('admin'));
			profile && (token.isVJQR = profile.realm_access.roles.includes('vjqr'));
			return token;
		},
		async session({ session, user, token }) {
			session.username = token.username;
			session.email = token.email;
			session.isAdmin = token.isAdmin;
			session.isVJQR = token.isVJQR;
			return session;
		},
	},
};

export { unstable_getServerSession as getServerSession } from 'next-auth';
