import 'next-auth';
import 'next-auth/jwt';

// Read more at: https://next-auth.js.org/getting-started/typescript#module-augmentation
// Can improve the typesafety based on what oauth gives, and what we're throwing around

declare module 'next-auth/jwt' {
	interface JWT {
		username: string;
		email: string;
		isAdmin: boolean;
		isVJQR: boolean;
	}
}

declare module 'next-auth' {
	// Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
	interface Session {
		username: string;
		email: string;
		isAdmin: boolean;
		isVJQR: boolean;
	}

	// Extended based on what oauth provides, and what ends up inside 'profile' when handling the jwt callbacks
	// TODO: this is based on local keycloak, test against platform one to verify values
	interface Profile {
		// exp: number;
		// iat: number;
		// auth_time: number;
		// jti: string;
		// iss: string;
		// aud: string;
		// sub: string;
		// typ: string;
		// azp: string;
		// session_state: string;
		// at_hash: string;
		// acr: string;
		// sid: string;
		// email_verified: boolean;
		realm_access: { roles: string[] };
		name: string;
		preferred_username: string;
		given_name: string;
		family_name: string;
		email: string;
	}
}
