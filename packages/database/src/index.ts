import { prisma } from '../prisma';
import { commit } from './commit';
import { deleteRows } from './delete';
import { seed } from './seed';

// Don't expose raw query capabilities to other packages
export const db = {
	courses: prisma.course,
	ksats: prisma.ksat,
	linkedResources: prisma.linkedResource,
	modules: prisma.module,
	proficiencies: prisma.proficiency,
	sequences: prisma.sequence,
	sources: prisma.source,
	units: prisma.unit,
	workroles: prisma.workrole,
	vjqrs: prisma.vJQR,
	commit,
	deleteRows,
	seed,
};
