import { Gitlab } from '@gitbeaker/node';
import { prisma } from '../prisma';

const token = process.env.GITLAB_API_TOKEN || '';
const contentProjectId = process.env.GITLAB_CONTENT_PROJECT_ID || '';

const gitlab = new Gitlab({
	host: 'https://gitlab.com',
	token,
	requestTimeout: 10000,
});

export const commit = async (options?: { msg?: string }) => {
	// TODO: msg based on frontend input
	// TODO: author based on session username?

	const commitMsg = options?.msg ?? 'update-commit';

	const result = await gitlab.Commits.create(contentProjectId, 'main', commitMsg, [
		{
			action: 'update',
			filePath: '_assessments.json',
			content: JSON.stringify(await prisma.$queryRaw`SELECT * FROM public."_assessments"`, null, 2),
		},
		{
			action: 'update',
			filePath: '_courseContent.json',
			content: JSON.stringify(await prisma.$queryRaw`SELECT * FROM public."_courseContent"`, null, 2),
		},
		{
			action: 'update',
			filePath: '_ksatToModules.json',
			content: JSON.stringify(await prisma.$queryRaw`SELECT * FROM public."_KsatToModule"`, null, 2),
		},
		{
			action: 'update',
			filePath: '_practice.json',
			content: JSON.stringify(await prisma.$queryRaw`SELECT * FROM public."_practice"`, null, 2),
		},
		{
			action: 'update',
			filePath: '_resources.json',
			content: JSON.stringify(await prisma.$queryRaw`SELECT * FROM public."_resources"`, null, 2),
		},
		{
			action: 'update',
			filePath: 'courses.json',
			content: JSON.stringify(await prisma.course.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'ksats.json',
			content: JSON.stringify(await prisma.ksat.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'linkedResources.json',
			content: JSON.stringify(await prisma.linkedResource.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'modules.json',
			content: JSON.stringify(await prisma.module.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'proficiencies.json',
			content: JSON.stringify(await prisma.proficiency.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'sequences.json',
			content: JSON.stringify(await prisma.sequence.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'sources.json',
			content: JSON.stringify(await prisma.source.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'units.json',
			content: JSON.stringify(await prisma.unit.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'workroles.json',
			content: JSON.stringify(await prisma.workrole.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
		{
			action: 'update',
			filePath: 'vjqrs.json',
			content: JSON.stringify(await prisma.vJQR.findMany({ orderBy: { id: 'asc' } }), null, 2),
		},
	]);

	console.table(JSON.stringify(result));

	console.log(`📝  The commit command has been executed.`);
};

if (require.main === module) {
	commit({ msg: 'Manual Commit From package.json script!' });
}
