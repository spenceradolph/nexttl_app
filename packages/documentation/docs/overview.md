---
sidebar_position: 1
---

# Overview

This application provides access to the _Master Training Task List_ (**MTTL**) explicating work role expectations across the USAF, and especially for the 90th Cyber Operations Squadron, as defined by US Cyber Command and other relevant stakeholders.

The **MTTL**

-   provides a listing of the _Knowledge_, _Skills_, _Abilities_, and _Tasks_ (**_KSATs_**) that should be mastered for various workroles across the 90th and other USAF Squadrons.
-   provides an overview of work role requirements and mappings to training and evaluation resources related to those work role requirements.
-   is intended as a resource for those designing and implementing training and evaluation i.e. at the 39th and the 90th.
-   can also support flight commanders in understanding crew readiness, capabilities, and training expectations given work role requirements.
